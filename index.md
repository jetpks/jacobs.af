---
layout: article
title: Jacobs AF
show_title: false
mode: immersive
header:
  theme: forest
article_header:
  type: cover
  image:
    src: /assets/images/cover1.jpg
---

# Welcome

Hi you've reached the website of [Eric Jacobs](/eric.html). I'm not here right
now, but please leave a message after the tone and I'll be back with you as
soon as I can.
