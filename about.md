---
title: About
layout: article
mode: immersive
header:
  theme: dark
article_header:
  type: cover
  image:
    src: /assets/images/lucy.jpg
---

This website exists primarily to satisfy the curiosity of people who enter
jacobs.af into their browser after seeing an email address ending in
"@jacobs.af". Yes, it's a real email address
