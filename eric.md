---
layout: article
title: Eric Jacobs
mode: immersive
header:
  theme: dark
article_header:
  type: overlay
  theme: dark
  background_color: "#203028"
  background_image:
    gradient: "linear-gradient(135deg, rgba(34, 139, 87 , .4), rgba(139, 34, 139, .4))"
    src: /assets/images/cover1.jpg
---

# Hi, I'm Eric

![Image](/assets/images/eric.jpg){:.rounded}

## Bio

I write software for a living.

In my free time I like to work on my Jeep and sometimes drive it over rocks.
Here's what that looks like:

![Image](/assets/images/jeep.jpg){:.rounded}

## Software

I write a lot of [Ruby](https://www.ruby-lang.org/en/) and a lot of
[Golang](https://golang.org/), but I'll write in just about anything if it
gets the job done.


